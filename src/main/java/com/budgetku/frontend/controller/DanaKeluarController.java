package com.budgetku.frontend.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/danakeluar")
public class DanaKeluarController {
    final String REDIRECT_URL = "https://budgetku-backend.herokuapp.com/";

    @GetMapping
    public String showDanaKeluarForm(@CookieValue(value = "email", defaultValue="") String emailCookie){
        if (emailCookie.equals("") || emailCookie.equals("anonymousUser")) {
            return String.format("redirect:%s/login", REDIRECT_URL);
        }
        return "dana-keluar";
    }

}
