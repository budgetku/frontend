package com.budgetku.frontend.controller;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.core.Authentication;
// import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/kategori")
public class KategoriController {
    final String REDIRECT_URL = "https://budgetku-backend.herokuapp.com/";

    @GetMapping("")
    public String getKategori(Model model, @CookieValue(value="email", defaultValue="") String email) {
        if (email.equals("") || email.equals("anonymousUser")) {
            return String.format("redirect:%s/login", REDIRECT_URL);
        }
        model.addAttribute("id", email);
        return "kategori";
    }

    @GetMapping("/list")
    public String listKategori(@CookieValue(value = "email", defaultValue="") String emailCookie) {
        if (emailCookie.equals("") || emailCookie.equals("anonymousUser")) {
            return String.format("redirect:%s/login", REDIRECT_URL);
        }
        return "list-kategori";
    }

}
