package com.budgetku.frontend.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class MainController {
    // Change this
    final String REDIRECT_URL = "https://budgetku-backend.herokuapp.com/";

    @GetMapping("/login")
    public String login() {
        return String.format("redirect:%s/login", REDIRECT_URL);
    }

    @GetMapping("/logout")
    public String logout(HttpServletResponse response) {
        Cookie cookie = new Cookie("email", "");
        response.addCookie(cookie);
        return String.format("redirect:%s/logout", REDIRECT_URL);
    }

    @GetMapping("/register")
    public String register() {
        return String.format("redirect:%s/register", REDIRECT_URL);
    }

    @GetMapping("/redirected")
    public String redirected(@ModelAttribute("email") String email, HttpServletResponse response) {
        if (email.equals("anonymousUser")) {
            return String.format("redirect:%s/login", REDIRECT_URL);
        }

        Cookie cookie = new Cookie("email", email);
        response.addCookie(cookie);
        return "redirect:/";
    }

    @GetMapping("/")
    public String home(@CookieValue(value = "email", defaultValue="") String emailCookie) {
        if (emailCookie.equals("") || emailCookie.equals("anonymousUser")) {
            return String.format("redirect:%s/login", REDIRECT_URL);
        }
        
        return "index";
    }

}
