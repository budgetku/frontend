package com.budgetku.model;

import java.util.ArrayList;
import java.util.List;

public class User {
  private long id;
  private String firstName;
  private String lastName;
  private String email;
  private String password;
  private String danaKeluarList;
  private List<String> budgetList = new ArrayList<>();
  private String roles;
  private List<String> kategoriList = new ArrayList<>();

  public User(long id, String firstName, String lastName, String email, String password, String roles) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
    this.danaKeluarList = null;
    this.roles = roles;
  }

  public String getEmail() {
    return this.email;
  }

  public String toString() {
    return email;
  }
}
