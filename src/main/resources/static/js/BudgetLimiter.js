const BASE_URL = "https://budgetku-backend.herokuapp.com/"
// const BASE_URL = "http://localhost:8090";
const EMAIL = Cookies.get("email");

function convertToJSON(formData) {
    let res = {};

    $.map(formData, function(n){
        res[n["name"]] = n["value"];
    });

    console.log(res);

    return res;
}

function populateYears() {
    let date = new Date();
    let year = date.getFullYear();
    let yearSelect = document.querySelector('#year');

    for (let i = 0; i <= 50; i++) {
        let option = document.createElement('option');
        option.textContent = year + i;
        yearSelect.appendChild(option);
    }
}

$(function() {
    populateYears();
    $.ajax({
        url: `${BASE_URL}/kategori/${EMAIL}`,
        method: "GET",
        success: (data) => {

            console.log(data);

            $("#category_id option[value='loading']").hide();

            $.each(data, function(i, v) {
                $("#category_id").append(new Option(v.nama, v.id));
            })
        },
        failure: (error) => {
            console.log(error);
            console.log("Unfortunately there's an error");
            $("#category_id option[value='loading']").hide();
            $("#category_id option[value='nil']").hide();
            $("#category_id").append(new Option("Failure in getting data", "nil"));
        }
    })

    $("#budget_form").submit(function(e) {
        e.preventDefault();
        let formData = $("#budget_form").serializeArray();

        $.ajax({
            url: `${BASE_URL}/budget/create/${EMAIL}`,
            method: "POST",
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(convertToJSON(formData)),
            success: (data) => {
                console.log(data)
                document.getElementById("budget_form").reset();
            },
            failure: () => {
                alert("There is a failure!")
            }
        });
    });
});
