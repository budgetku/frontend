const EMAIL = getCookie("email");
// const BASE_URL = "http://localhost:8090"
const BASE_URL = "https://budgetku-backend.herokuapp.com/"

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

// function convertToJSON(element) {
//     console.log(JSON.stringify(element, null, '  '))
//     return JSON.stringify(element, null, '  ')
// }

function convertToJSON(formData) {
    let res = {};

    $.map(formData, function(n){
        res[n["name"]] = n["value"];
    });

    console.log(res);

    return res;
}

$(function() {
    $.ajax({
        url: `${BASE_URL}/budget/list/${EMAIL}`,
        method: "GET",
        success: (data) => {

            console.log(data);

            $("#budget_id option[value='loading']").hide();

            $.each(data, function(i, v) {
                $("#budget_id").append(new Option(`[${v.kategori.nama}] ${v.deskripsi} (${v.bulanBerakhir} ${v.tahunBerakhir})`, v.id));
            })
        },
        failure: (error) => {
            console.log(error);
            console.log("Unfortunately there's an error");
            $("#category_id option[value='nil']").hide();
            $("#category_id").append(new Option("Failure in getting data", "nil"));
        }
    })

    $("#danakel_form").submit(function (e) {
        e.preventDefault();
        // let formData = {
        //     nominal: $("#nominal").val(),
        //     tanggal: $("#tanggal").val(),
        //     deskripsi: $("#description_id").val(),
        //     kategori: {
        //         idKategori: $("#category_id").val()
        //     }
        // };
        let formData = $("#danakel_form").serializeArray();
        console.log(convertToJSON(formData))

        $.ajax({
            url: `${BASE_URL}/danakeluar/create/${EMAIL}`,
            method: "POST",
            data: JSON.stringify(convertToJSON(formData)),
            contentType: "application/json",
            success: (data) => {
                console.log(data)
                $("#budget_id option[value='loading']").hide();
                // alert("success")
            },
            failure: () => {
                alert("There is a failure!")
            }
        });
    });
});

