// const BASE_URL = "http://localhost:8090";
const BASE_URL = "https://budgetku-backend.herokuapp.com/"
const EMAIL = Cookies.get("email");

// function convertToJSON(element) {
//     console.log(JSON.stringify(element, null, ' '))
//     return JSON.stringify(element, null, ' ')
// }

function convertToJSON(formData) {
    let res = {};

    $.map(formData, function(n){
        res[n["name"]] = n["value"];
    });

    console.log(res);

    return res;
}

$(function () {
    $("#kategori_form").submit(function (e) {
        e.preventDefault();
        // let formData = {
        //     namaKategori: $("#namaKategori").val(),
        //     deskripsi: $("#deskripsi").val()
        // }
        let formData = $("#kategori_form").serializeArray();

        $.ajax({
            url: `${BASE_URL}/kategori/create/${EMAIL}`,
            method: "POST",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(convertToJSON(formData)),
            success: (data) => {
                console.log(data)
                document.getElementById("kategori_form").reset();
            },
            failure: () => {
                alert("There is a failure!");
            }
        });
    });

    $.ajax({
        url: `${BASE_URL}/kategori/${EMAIL}`,
        method: "GET",
        contentType: "application/json",
        success: (data) => {
            console.log(data);
            if (data.length === 0) {
                const kosong = '<h3 class="text-center text-info" id="kosong">List kategori belum ada</h3>';
                $('div.kategori-result').append(kosong);
            } else {
                for (let i = 0; i < data.length; i++) {
                    let clone = $("#kategori-card-template").contents().clone(true);
                    clone.find('h3').text(data[i].nama);
                    clone.find('p').text(data[i].deskripsi);
                    $('div.kategori-result').append(clone);
                }
            }
        },
        failure: (e) => {
            console.log(e);
        }
    });
});
