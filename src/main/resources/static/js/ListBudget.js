// const BASE_URL = "http://localhost:8090";
const BASE_URL = "https://budgetku-backend.herokuapp.com/"
const EMAIL = Cookies.get("email");

function formatRupiah(angka){
    let prefix = "Rp. ";
    if (angka[0] === "-") {
        prefix = "-Rp. "
    }
    let number_string = angka.replace(/[^,\d]/g, '').toString(),
	split   		= number_string.split(','),
	sisa     		= split[0].length % 3,
	rupiah     		= split[0].substr(0, sisa),
	ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

    if (ribuan) {
        let separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }
 
	rupiah = split[1] !== undefined ? rupiah + ',' + split[1] : rupiah;
	return prefix + rupiah;
}

$(function () {
    $.ajax({
        url: `${BASE_URL}/budget/list-state/${EMAIL}`,
        method: "GET",
        contentType: "application/json",
        success: (data) => {
            console.log(data);
            if (data.length === 0) {
                const kosong = '<h3 class="text-center dark-green">List budget belum ada</h3>';
                $('div.budget-result').append(kosong);
            } else {
                for (let i = 0; i < data.length; i++) {
                    let clone = $("#budget-card-template").contents().clone(true);
                    clone.find('h4').text(data[i][0].kategori.nama);
                    clone.find('h6').text(data[i][0].bulanBerakhir + " - " + data[i][0].tahunBerakhir);
                    clone.find('.nominal').text(formatRupiah(data[i][0].nominal.toString()));
                    clone.find('.desc').text(data[i][0].deskripsi);
                    clone.find('.summary').text(data[i][1]);

                    if (data[i][1] === "On Limit!") {
                        clone.find('.summary').addClass("text-danger");
                        clone.find('.nominal').addClass("text-danger");
                    } else if (data[i][1] === "Expired") {
                        clone.find('.summary').addClass("text-warning");
                        clone.find('.nominal').addClass("text-warning");
                    } else if (data[i][1] === "Available") {
                        clone.find('.summary').addClass("text-success");
                        clone.find('.nominal').addClass("text-success");
                    }

                    $('div.budget-result').append(clone);
                }
            }
        },
        failure: (e) => {
            console.log(e);
        }
    });
});